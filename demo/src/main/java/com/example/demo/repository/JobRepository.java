package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Job;
import java.util.List;


@Repository
public interface JobRepository extends JpaRepository<Job ,Long> {
    
    Optional<Job>  findById(Long id);
}
