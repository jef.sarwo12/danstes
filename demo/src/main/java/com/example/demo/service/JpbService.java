package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Job;
import com.example.demo.repository.JobRepository;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class JpbService {
    @Autowired
    protected JobRepository jobRepository;

    public List<Job> geList(){
        return jobRepository.findAll();
    }
}
