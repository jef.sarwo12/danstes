package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class UserService {

    @Autowired
    protected UserRepository userRepository;

    public List<User> getList(){
        return userRepository.findAll();
    }

    // User saveUser(User user);

    // List<User> fetchUser();

    // User getUserById(int id);

    // User updateUserById(int id, User user);

    // String deleteUserById(int id);
}
