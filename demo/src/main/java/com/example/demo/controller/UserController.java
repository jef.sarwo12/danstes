package com.example.demo.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.tomcat.util.file.ConfigurationSource.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Job;
import com.example.demo.entity.User;
import com.example.demo.repository.JobRepository;
import com.example.demo.repository.UserRepository;
// import com.example.demo.service.UserService;

@RestController
public class UserController {
    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected JobRepository jobRepository;

    
    // @Autowired 
    // private UserService userService;

    //Login API
    @PostMapping("/login")
    public ResponseEntity<Object> loginUser(@RequestBody User user){
        try {
            String username =  user.getUsername();
            String password = user.getPassword();

            Optional<User> dataUser = this.userRepository.findByUsername(username);
            if (dataUser.isPresent()) {
                if (password.equals(dataUser.get().getPassword())) {
			        return new ResponseEntity<>(dataUser, HttpStatus.OK);   
                } else {
			        return new ResponseEntity<>("Password Salah", HttpStatus.OK);
                }
            } else {
                return new ResponseEntity<>("User Tidak Ditemukan", HttpStatus.OK);    
            }

        } catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    //Get job list API
    @GetMapping(value="/api/recruitment/positions")
    public ResponseEntity<List<Job>> getJobs() {
        List<Job> listJobById = this.jobRepository.findAll();
        return new ResponseEntity<>(listJobById, HttpStatus.OK);
    }

    //Get job detail API
    @GetMapping("/api/recruitment/positions/{id}")
    public ResponseEntity<Optional<Job>> getJobByid(@PathVariable("id") Long id){
        
        Optional<Job> listJobById = this.jobRepository.findById(id);
        return new ResponseEntity<>(listJobById, HttpStatus.OK);
    }

    //Download job list API
     @GetMapping(value="/api/recruitment/positionsCSV", produces = "text/csv")
    public ResponseEntity<Object> getJobsCSV() {
        String[] csvHeader = {
                "id", "type", "url","company","companyUrl","location","tittle","description","createdAt","howToApply","companyLogo"
        };
        List<Job> listJob = this.jobRepository.findAll();
        ByteArrayInputStream byteArrayOutputStream;

        try(
             ByteArrayOutputStream out = new ByteArrayOutputStream();
            // defining the CSV printer
            CSVPrinter csvPrinter = new CSVPrinter(
                    new PrintWriter(out),
                    // withHeader is optional
                    CSVFormat.DEFAULT.withHeader(csvHeader)
            );
        ) {
            
        for (Job record : listJob)
            csvPrinter.printRecord(record.getId(),record.getType(),record.getUrl(),record.getCompany(),record.getCompanyUrl(),record.getLocation(),record.getTitle(),record.getDescription(),record.getCreatedAt(),record.getHowToApply(),record.getCompanyLogo());

        // writing the underlying stream
        csvPrinter.flush();

        byteArrayOutputStream = new ByteArrayInputStream(out.toByteArray());
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        InputStreamResource fileInputStream = new InputStreamResource(byteArrayOutputStream);

        String csvFileName = "Jobs.csv";

          // setting HTTP headers
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + csvFileName);
        // defining the custom Content-Type
        headers.set(HttpHeaders.CONTENT_TYPE, "text/csv");


        return new ResponseEntity<>(
            fileInputStream,
            headers,
            HttpStatus.OK
        );
    }
}
