package com.example.demo.entity;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "job")
public class Job {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id")
	private Long id;

	@Column(name = "type", length = 100)
	private String type;

	@Column(name = "url", length = 255)
	private String url;

	@Column(name = "company", length = 150)
	private String company;

	@Column(name = "company_url", length = 255)
	private String companyUrl;

	@Column(name = "location", length = 100)
	private String location;

    @Column(name = "title", length = 255)
	private String title;
	
    @Column(name = "description")
	private String description;

	@Column(name = "created_at")
    private Date createdAt;
    
    @Column(name = "how_to_apply")
	private String howToApply;

    @Column(name = "company_logo")
	private String companyLogo;
    



    /**
     * @return Long return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return String return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return String return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return String return the company
     */
    public String getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(String company) {
        this.company = company;
    }

    /**
     * @return String return the companyUrl
     */
    public String getCompanyUrl() {
        return companyUrl;
    }

    /**
     * @param companyUrl the companyUrl to set
     */
    public void setCompanyUrl(String companyUrl) {
        this.companyUrl = companyUrl;
    }

    /**
     * @return String return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return String return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return String return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return Date return the createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return String return the howToApply
     */
    public String getHowToApply() {
        return howToApply;
    }

    /**
     * @param howToApply the howToApply to set
     */
    public void setHowToApply(String howToApply) {
        this.howToApply = howToApply;
    }

    /**
     * @return String return the companyLogo
     */
    public String getCompanyLogo() {
        return companyLogo;
    }

    /**
     * @param companyLogo the companyLogo to set
     */
    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

}
